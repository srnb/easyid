# easyid

Cross-platform Scala library to Base85 UUIDs

## Usage

```scala
lazy val easyid = ProjectRef(uri("https://gitlab.com/srnb/easyid.git"), "easyid")

// ...

lazy val myProject = /*
  ...
*/ .dependsOn(easyid)
```