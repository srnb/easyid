// shadow sbt-scalajs' crossProject and CrossType from Scala.js 0.6.x
import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val sharedSettings = Seq(
  scalaVersion := "2.11.12",
  organization := "tf.bug",
  name := "easyid",
  version := "0.1.0"
)

lazy val easyid =
  // select supported platforms
  crossProject(JSPlatform, JVMPlatform, NativePlatform)
    .crossType(CrossType.Full) // [Pure, Full, Dummy], default: CrossType.Full
    .in(file("."))
    .settings(sharedSettings)
    .jsSettings(scalaJSModuleKind := ModuleKind.CommonJSModule) // defined in sbt-scalajs-crossproject
    .jvmSettings(/* ... */)
    .nativeSettings(/* ... */) // defined in sbt-scala-native

// Optional in sbt 1.x (mandatory in sbt 0.13.x)
lazy val easyidJS     = easyid.js
lazy val easyidJVM    = easyid.jvm
lazy val easyidNative = easyid.native

