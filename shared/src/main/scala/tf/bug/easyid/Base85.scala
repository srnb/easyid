package tf.bug.easyid

object Base85 {

  val encoding = "0123456789abcdef" +
    "ghijklmnopqrstuv" +
    "wxyzABCDEFGHIJKL" +
    "MNOPQRSTUVWXYZ.-:" +
    "+=^!/*?&<>()[]{}@%$#"

  val decoding = Array[Byte](
    0x00, 0x44, 0x00, 0x54, 0x53, 0x52, 0x48, 0x00, 0x4B, 0x4C, 0x46, 0x41,
    0x00, 0x3F, 0x3E, 0x45, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x40, 0x00, 0x49, 0x42, 0x4A, 0x47, 0x51, 0x24, 0x25, 0x26,
    0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x32,
    0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D, 0x4D,
    0x00, 0x4E, 0x43, 0x00, 0x00, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
    0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C,
    0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x4F, 0x00, 0x50, 0x00, 0x00
  )

  def encode(data: Array[Byte]): String = {
    val padded = data.grouped(4).map(_.padTo(4, 0.toByte))
    padded.map(encodeSegment).mkString
  }

  def encodeSegment(data: Array[Byte]): String = {
    if (data.length != 4)
      throw new IllegalArgumentException("Segment must be 4 bytes in length")
    val hv: Long = data.foldLeft(0l)((i, b) => (i * 256l) + (b & 0xFF))
    val a = (0 to 4)
      .map((e: Int) => Math.pow(85, e).toInt)
      .reverse
      .foldLeft("")((acc, d) => {
        val index = (hv / d) % 85
        acc + encoding(index.toInt)
      })
    a
  }

  def decode(str: String): Array[Byte] = {
    val padded = str.grouped(5).map(_.padTo(5, ' '))
    padded.map(decodeSegment).foldLeft(Array[Byte]())((a1, a2) => a1 ++ a2)
  }

  def decodeSegment(str: String): Array[Byte] = {
    if (str.length != 5)
      throw new IllegalArgumentException("Segment must be 5 bytes in length")
    val hv = str
      .getBytes("UTF-8")
      .foldLeft(0l)((i, b) => {
        val index = (b & 0xFF) - 32
        (i * 85) + decoding(index)
      })
    val a = (0 to 3)
      .map(e => Math.pow(256, e).toInt)
      .reverse
      .foldLeft(Array[Byte]())((arr, d) => {
        val vl = (hv / d) % 256
        arr :+ vl.toByte
      })
    a
  }

}
