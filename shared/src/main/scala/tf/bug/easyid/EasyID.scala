package tf.bug.easyid

import java.util.{Base64, UUID}

case class EasyID private (fuid: String) {

  def toUUID: UUID = {
    val bytes = Base85.decode(fuid)
    val str = bytes
      .map(_ & 0xFF)
      .map(_.toHexString.reverse.padTo(2, "0").reverse.mkString)
      .mkString
    UUID.fromString(
      new StringBuilder(str)
        .insert(20, '-')
        .insert(16, '-')
        .insert(12, '-')
        .insert(8, '-')
        .toString)
  }

}

object EasyID {

  def apply(u: UUID): EasyID = {
    val bytes = u
      .toString()
      .replaceAll("-", "")
      .grouped(2)
      .map((s: String) => Integer.parseInt(s, 16).toByte)
    val b85 = Base85.encode(bytes.toArray)
    EasyID(b85)
  }

}
