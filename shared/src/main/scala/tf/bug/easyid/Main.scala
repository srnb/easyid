package tf.bug.easyid

import java.util.UUID

object Main {

  def main(args: Array[String]): Unit = {
    val uuids = args.map(UUID.fromString)
    val easyids = uuids.map(EasyID.apply)
    easyids.foreach(i => println(s"<~${i.fuid}~>"))
  }

}
