package tf.bug.easyid

import java.util.UUID
import scala.scalajs.js
import scala.scalajs.js.annotation._

@JSExportTopLevel("EasyID")
object EasyIDExport {

  @JSExport
  def fromUUID(u: UUID): EasyID = EasyID(u)

  @JSExport
  def toUUID(e: EasyID): UUID = e.toUUID

}

@JSExportTopLevel("Base85")
object Base85Export {

  @JSExport
  def encode(data: Array[Byte]): String = Base85.encode(data)

  @JSExport
  def decode(str: String): Array[Byte] = Base85.decode(str)

}
